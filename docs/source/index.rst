ja_webutils - a Python package for building HTML docments
=========================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
    modules.rst
    ja_webutils.rst

.. include::
    overview.rst

.. autosummary::
    :toctree: generated
    :caption: API
    :recursive:


Page class top container for HTML
+++++++++++++++++++++++++++++++++

.. automodule:: ja_webutils.Page
     :members:
     :inherited-members:

PageItem - encapsulates non form input tags
+++++++++++++++++++++++++++++++++++++++++++

.. automodule:: ja_webutils.PageItem
    :members:
    :inherited-members:

PageForm - encapsulates input tags
++++++++++++++++++++++++++++++++++

.. automodule:: ja_webutils.PageForm
     :members:
     :inherited-members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
